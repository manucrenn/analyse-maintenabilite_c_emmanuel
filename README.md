# analyse-maintenabilite_c_emmanuel

## Table des matières
- [Exercice 1 - Questions sur les acquis, notions vues en cours](#exercice-1---questions-sur-les-acquis-notions-vues-en-cours)
- [Exercice 2 - Refactoring](#exercice-2---refactoring)
    - [Installer et lancer le projet](#installer-et-lancer-le-projet)
    - [Exécuter les tests](#exécuter-les-tests)
    - [Commentaires](#commentaires)

## Exercice 1 - Questions sur les acquis, notions vues en cours
1. **Quelles sont les principales sources de complexité dans un système logiciel (sources d’un programme) ?**
  

Les principales sources de complexité dans un système logiciel incluent :

   - Complexité algorithmique : La difficulté inhérente des algorithmes utilisés.
   - Couplage élevé : Trop de dépendances entre les différentes parties du code.
   - Cohésion faible : Les classes ou les modules remplissent plusieurs rôles.
   - Taille du code : Le volume du code peut rendre la compréhension et la maintenance difficiles.
   - Concurrence : La gestion des processus parallèles et des threads.
   - Changements fréquents : Les modifications constantes des exigences du client.
   - Héritage et polymorphisme : Un usage excessif peut rendre le suivi du flux du programme compliqué.

2. **Quel(s) avantage(s) procure le fait de programmer vers une interface et non vers une implémentation ?**


Programmer vers une interface et non vers une implémentation procure plusieurs avantages :

   - Flexibilité : Permet de changer l'implémentation sans affecter le reste du code.
   - Réutilisabilité : Facilite la réutilisation des composants.
   - Testabilité : Simplifie les tests unitaires en permettant de substituer facilement des mocks ou des stubs.

3. **Comment comprenez-vous chaque étape de l'heuristique d'Eric Steven Raymond ?**
   

L'heuristique d'Eric Steven Raymond est une série de règles qui permettent de déterminer si un code est de bonne qualité. Chaque étape de l'heuristique est conçue pour évaluer un aspect spécifique du code. Voici comment je comprends chaque étape :

   - Fonctionnalité : Le code fait-il ce qu'il est censé faire ?
   - Lisibilité : Le code est-il facile à lire et à comprendre ?
   - Cohérence : Le code suit-il des conventions de style cohérentes ?
   - Modularité : Le code est-il divisé en modules ou en classes logiques ?
   - Documentation : Le code est-il bien documenté ?
   - Testabilité : Le code est-il facile à tester ?
   - Performance : Le code est-il optimisé pour les performances ?
   - Sécurité : Le code est-il sécurisé contre les attaques ?

4. **Quelle méthode ou approche est recommandée pour mener à bien un travail de refactoring ?**
   

La méthode recommandée pour mener à bien un travail de refactoring est la suivante :

   - Identifier les parties du code qui nécessitent un refactoring.
   - Définir les objectifs du refactoring (améliorer la lisibilité, réduire la complexité, etc.).
   - Créer des tests unitaires pour les parties du code à refactoriser.
   - Effectuer les modifications nécessaires en respectant les principes SOLID.
   - Vérifier que les tests unitaires passent après le refactoring.
   - Documenter les changements effectués et les raisons derrière ces changements.

## Exercice 2 - Refactoring

### Installer et lancer le projet

Pour compiler et exécuter les sources Java :

Utiliser Java 21.0.1

```sh
javac -d bin src/main/java/com/mspr/*.java
java -cp bin com.mspr.Main
```

### Exécuter les tests

Pour exécuter les tests unitaires :

```sh
javac -cp "bin;lib/junit-jupiter-api-5.7.0.jar;lib/junit-jupiter-engine-5.7.0.jar;lib/apiguardian-api-1.1.0.jar;lib/opentest4j-1.2.0.jar" -d bin src/test/java/com/mspr/*.java
java -cp "bin;lib/junit-jupiter-api-5.7.0.jar;lib/junit-jupiter-engine-5.7.0.jar;lib/apiguardian-api-1.1.0.jar;lib/opentest4j-1.2.0.jar" org.junit.jupiter.api.Test
```

### Commentaires

- La première étape a été de convertir le code php en code java.
- Deuxièmement j'ai écrit un test pour pouvoir vérifier que le code fonctionne correctement, en les maintenant à jour dès que je faisais un changement.
- J'ai ensuite Utilisé une interface Vehicule pour pouvoir avoir plusieurs type de véhicule. Sans avoir les constants dans la Class Car. 
Cela permet également de pouvoir avoir le calcul de la location en fonction du type de véhicule directement dans les class concerné et ne plus avoir besoin du switch dans la class Customer.
Cela permet de pouvoir rajouter un type de véhicule facilement sans avoir à modifier la class Customer. 
En effet il suffit de créer une nouvelle class qui implements Vehicle et y mettre la logique de calcul de la location,
de cette manière il n'y a que un seul endroit à modifier plutot que que changer la class Customer et la class Car.
- J'ai ensuite Ajouté une fonction invoiceJSON dans la class Customer pour pouvoir générer un JSON de la facture.
- Puis j'ai ajouté le calcule des points de fidelités.
- Et enfin j'ai utilisé sonarLint pour corriger les erreurs et les warnings.