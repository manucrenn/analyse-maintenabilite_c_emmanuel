package com.mspr;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerTest {

    @Test
    void testInvoice() {
        // Create cars
        Car car1 = new Car("Car 1", new RegularCar());
        Car car2 = new Car("Car 2", new NewModelCar());

        // Create rentals
        Rental rental1 = new Rental(car1, 3);
        Rental rental2 = new Rental(car2, 2);

        // Create customer and add rentals
        Customer customer = new Customer("John Doe");
        customer.addRental(rental1);
        customer.addRental(rental2);

        // Expected invoice
        String expectedInvoice = "Rental Record for John Doe\n" +
                "\tCar 1\t335.0\n" +
                "\tCar 2\t390.0\n" +
                "Amount owed is 725,0\n" +
                "You earned 76 frequent renter points\n";

        // Assert invoice
        assertEquals(expectedInvoice, customer.invoice());
    }

    @Test
    void testInvoiceWithLongerRentalPeriod() {
        // Create cars
        Car car1 = new Car("Car 1", new RegularCar());
        Car car2 = new Car("Car 2", new NewModelCar());

        // Create rentals with longer periods
        Rental rental1 = new Rental(car1, 6);
        Rental rental2 = new Rental(car2, 4);

        // Create customer and add rentals
        Customer customer = new Customer("John Doe");
        customer.addRental(rental1);
        customer.addRental(rental2);

        // Expected invoice
        String expectedInvoice = "Rental Record for John Doe\n" +
                "\tCar 1\t220.0\n" +
                "\tCar 2\t490.0\n" +
                "Amount owed is 710,0\n" +
                "You earned 74 frequent renter points\n";

        // Assert invoice
        assertEquals(expectedInvoice, customer.invoice());
    }

}
