package com.mspr;

public interface Vehicle {
    double calculateRentalCost(int daysRented);
}
