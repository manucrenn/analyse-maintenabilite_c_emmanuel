package com.mspr;

public class RegularCar implements Vehicle{
    @Override
    public double calculateRentalCost(int daysRented) {
        double cost = 5000.0 + daysRented * 9500;
        if(daysRented > 5) {
            cost -= (daysRented - 2) * 10000;
        }
        return cost;
    }
}
