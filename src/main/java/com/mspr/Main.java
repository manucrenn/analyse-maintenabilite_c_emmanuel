package com.mspr;

// Exemples d'utilisation :
public class Main {
    public static void main(String[] args) {
        Car car1 = new Car("Car 1", new RegularCar());
        Car car2 = new Car("Car 2", new NewModelCar());

        Rental rental1 = new Rental(car1, 3);
        Rental rental2 = new Rental(car2, 2);

        Customer customer = new Customer("John Doe");
        customer.addRental(rental1);
        customer.addRental(rental2);

        System.out.println(customer.invoice());
        System.out.println(customer.invoiceJSON());
    }
}

