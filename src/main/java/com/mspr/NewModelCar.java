package com.mspr;

public class NewModelCar implements Vehicle{
    @Override
    public double calculateRentalCost(int daysRented) {
        double cost = 9000.0 + daysRented * 15000;
        if(daysRented > 3) {
            cost -= (daysRented - 2) * 10000;
        }
        return cost;
    }
}
