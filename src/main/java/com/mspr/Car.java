package com.mspr;

public class Car {
        private final String title;
        private final Vehicle vehicleType;

        public Car(String title, Vehicle vehicleType) {
            this.title = title;
            this.vehicleType = vehicleType;
        }

        public Vehicle getVehicleType() {
            return vehicleType;
        }

        public String getTitle() {
            return title;
        }
}
