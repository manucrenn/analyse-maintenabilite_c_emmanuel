package com.mspr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Customer {
    private final String name;
    private final List<Rental> rentals;

    public Customer(String name) {
        this.name = name;
        this.rentals = new ArrayList<>();
    }

    public void addRental(Rental rental) {
        rentals.add(rental);
    }

    public String getName() {
        return name;
    }

    public String invoice() {
        double totalAmount = 0.0;
        int frequentRenterPoints = 0;
        StringBuilder result = new StringBuilder("Rental Record for " + getName() + "\n");

        for (Rental each : rentals) {
            double thisAmount = each.getCar().getVehicleType().calculateRentalCost(each.getDaysRented());

            // Add frequent renter points
            frequentRenterPoints++;

            // Add bonus for a two day new release rental
            if (each.getCar().getVehicleType() instanceof NewModelCar && each.getDaysRented() > 1) {
                frequentRenterPoints++;
            }

            // Show figures for this rental
            result.append("\t").append(each.getCar().getTitle()).append("\t").append(thisAmount / 100).append("\n");
            totalAmount += thisAmount;
        }

        frequentRenterPoints += (int)Math.ceil(totalAmount / 1000);


        // Add footer lines
        result.append("Amount owed is ").append(String.format("%.1f", totalAmount / 100)).append("\n");
        result.append("You earned ").append(frequentRenterPoints).append(" frequent renter points\n");

        return result.toString();
    }

    public String invoiceJSON() {
        double totalAmount = 0;
        int frequentRenterPoints = 0;
        Map<String, Object> invoice = new HashMap<>();
        invoice.put("Customer", getName());
        List<Map<String, Object>> rentalsList = new ArrayList<>();

        for (Rental each : rentals) {
            double thisAmount = each.getCar().getVehicleType().calculateRentalCost(each.getDaysRented());

            frequentRenterPoints++;

            if (each.getCar().getVehicleType() instanceof NewModelCar && each.getDaysRented() > 1) {
                frequentRenterPoints++;
            }

            Map<String, Object> rentalDetails = new HashMap<>();
            rentalDetails.put("Car", each.getCar().getTitle());
            rentalDetails.put("Amount", thisAmount / 100);
            rentalsList.add(rentalDetails);

            totalAmount += thisAmount;
        }

        frequentRenterPoints += (int)Math.ceil(totalAmount / 1000);


        invoice.put("Rentals", rentalsList);
        invoice.put("Total Amount", totalAmount / 100);
        invoice.put("Frequent Renter Points", frequentRenterPoints);

        return invoice.toString();
    }
}
